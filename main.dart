import 'package:flutter/material.dart';
import 'package:module4_assignment/main2.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: splash(),
        theme: ThemeData(
            primarySwatch: Colors.green,
            accentColor: Colors.green,
            scaffoldBackgroundColor: Color.fromARGB(255, 141, 207, 143),
            textTheme: TextTheme(
              bodyText2: TextStyle(color: Colors.white),
            ),
            elevatedButtonTheme: ElevatedButtonThemeData(
              style: ElevatedButton.styleFrom(
                  primary: Colors.green, onPrimary: Colors.white),
            ),
            textButtonTheme: TextButtonThemeData(
                style: TextButton.styleFrom(primary: Colors.white))));
  }
}
